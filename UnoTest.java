import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;
public class UnoTest {
    @Test
    public void testDeck() {
        Deck deck = new Deck();
    }

    @Test 
    public void addTest() {
        Deck deck = new Deck();
        deck.addToDeck(new WildCard());
    }

    @Test
        public void testShuffle() {
            Deck deck = new Deck();
            deck.shuffle();

        }
    @Test 
        public void testTwoWild() {
            WildCard wild1 = new WildCard();
            WildCard wild2 = new WildCard();
            assertTrue(wild1.canPlay(wild2));
        }

        @Test 
            public void testNumber() {
                NumberedCards card1 = new NumberedCards(5, "red");
                NumberedCards card2 = new NumberedCards(5, "green");
                assertTrue(card1.canPlay(card2));
            }

        @Test 
            public void testColor() {
                NumberedCards card1 = new NumberedCards(5, "red");
                ReverseCard card2 = new ReverseCard("red");
                assertTrue(card1.canPlay(card2));
            }

}
