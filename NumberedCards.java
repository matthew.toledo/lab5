public class NumberedCards extends UnoCard {
    private int number;

    public NumberedCards(int number, String color) {
        this.cardType = "number";
        this.number = number;
        this.color = color;
    }

    public int getNumber() {
        return this.number;
    }
}