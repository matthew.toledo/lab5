public abstract class UnoCard implements Card {
    protected String color;
    protected String cardType;

    public boolean canPlay(UnoCard card) {
        if (this.color.equals(card.getColor()) || this.color.equals("wild") || card.getColor().equals("wild")) {
            return true;
        }

        else if (this.cardType.equals("number") || ((NumberedCards)this).getNumber() == ((NumberedCards)card).getNumber() ) {
            return true;
        }

        return false;
    }
    
    public String getType() {
        return this.cardType;
    }

    public String getColor() {
        return this.color;
    }

}
